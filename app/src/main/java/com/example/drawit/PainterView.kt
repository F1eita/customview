package com.example.drawit

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import java.lang.Math.pow
import kotlin.math.*
import kotlin.properties.Delegates

class PainterView(context: Context, attrs: AttributeSet?) : View(context, attrs){


    constructor(context: Context) : this(context, null)


    private var colorLine by Delegates.notNull<Int>()
    private lateinit var figure: String
    private var drawPaint = Paint()
    private var path = Path()

    private var widthLine: Float = 7f

    var centerX by Delegates.notNull<Float>()
    var centerY by Delegates.notNull<Float>()
    var radius by Delegates.notNull<Float>()

    var lastX by Delegates.notNull<Float>()
    var lastY by Delegates.notNull<Float>()

    init {
        if (attrs != null){
            initAttribute(attrs)
        }
        else {
            initDefAttribute()
        }
        drawPaint.setColor(colorLine)
        drawPaint.setAntiAlias(true)
        drawPaint.setStrokeWidth(widthLine)
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }
    fun initAttribute(attrs: AttributeSet?){
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PainterView)
        figure = typedArray.getString(R.styleable.PainterView_figure) ?:"line"
        colorLine = typedArray.getColor(R.styleable.PainterView_colorLine, Color.BLACK)

        typedArray.recycle()
    }
    fun initDefAttribute(){
        figure = "line"
        colorLine = Color.BLACK
    }

    fun setFigure(figure: String){
        this.figure = figure
    }
    fun setColor(color: Int){
        this.colorLine = color
        drawPaint.setColor(colorLine)
    }
    fun offset(){
        figure = "inOffsetMode"
    }
    fun clearAll(){
        path.rewind()
    }


    override fun onDraw(canvas: Canvas?) {
        canvas?.drawPath(path, drawPaint)
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (figure){
            "line" -> {
                val pointX = event.x
                val pointY = event.y
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        path.moveTo(pointX, pointY)
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> path.lineTo(pointX, pointY)
                    else -> return false
                }
            }
            "straight_line" -> {
                val pointX = event.x
                val pointY = event.y
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        path.moveTo(pointX, pointY)
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                    }
                    MotionEvent.ACTION_UP -> {
                        path.lineTo(pointX, pointY)
                        return false
                    }
                    else -> return false
                }
            }
            "circle" -> {
                val pointX = event.x
                val pointY = event.y
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        path.moveTo(pointX, pointY)
                        centerX = pointX
                        centerY = pointY
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        path.moveTo(pointX, pointY)
                    }
                    MotionEvent.ACTION_UP -> {
                        path.moveTo(pointX, pointY)
                        radius = sqrt((centerX - pointX)*(centerX - pointX) + (centerY - pointY)*(centerY - pointY))
                        path.addCircle(centerX, centerY, radius, Path.Direction.CW)
                        return false
                    }
                    else -> return false
                }
            }
            "inOffsetMode"->{
                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> {
                            path.moveTo(event.x, event.y)
                            lastX = event.x
                            lastY = event.y
                            return true
                        }
                        MotionEvent.ACTION_MOVE -> {
                            path.offset((event.x - lastX), (event.y - lastY))
                            lastX = event.x
                            lastY = event.y
                        }
                        else -> return false
                    }
            }
        }
        postInvalidate()
        return true
    }
}