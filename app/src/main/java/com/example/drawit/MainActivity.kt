package com.example.drawit

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val paintV: PainterView = findViewById(R.id.painterView)
        val btnDrawLine: Button = findViewById(R.id.drawLine)
        val btnDrawCircle: Button = findViewById(R.id.drawCircle)
        val btnDrawSLine: Button = findViewById(R.id.drawStraightLine)
        val btnOffset: Button = findViewById(R.id.offset)
        val btnClearAll: Button = findViewById(R.id.clearAll)
        btnDrawLine.setOnClickListener {
            paintV.setFigure("line")
            paintV.setColor(Color.rgb(randomInt(),randomInt(),randomInt()))
        }
        btnDrawSLine.setOnClickListener {
            paintV.setFigure("straight_line")
            paintV.setColor(Color.rgb(randomInt(),randomInt(),randomInt()))
        }
        btnDrawCircle.setOnClickListener {
            paintV.setFigure("circle")
            paintV.setColor(Color.rgb(randomInt(),randomInt(),randomInt()))
        }
        btnOffset.setOnClickListener{
            paintV.offset()
        }
        btnClearAll.setOnClickListener {
            paintV.clearAll()
        }
    }

    fun randomInt(): Int{
        return (0..255).random()
    }

}